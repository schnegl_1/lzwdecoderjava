package at.debugged.starleaflzw.LZWDecoder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * LZW Decoder created for StarLeaf in order to convince the company of my skills and in order to get the chance to do
 * an internship.
 *
 * LZW explanation: https://www.youtube.com/watch?v=mxqD315rYnA and
 *                  https://en.wikipedia.org/wiki/Lempel%E2%80%93Ziv%E2%80%93Welch
 *
 * @author Martin Schneglberger
 */
public class LZWFileDecoder {

    private static int INIT_DICTIONARY_SIZE = 256;

    private byte[] mFile;
    private HashMap<Integer, String> mDictionary = new HashMap<>();
    private ArrayList<Integer> mCodes = new ArrayList<>();
    private boolean mOddNumberOfCodes;

    public LZWFileDecoder(String path) {
        readFile(path);
        initDictionaryWithAscii();
        mOddNumberOfCodes = isOddNumberOfBytes();
    }

    private boolean isOddNumberOfBytes() {
        return mFile.length % 3 != 0;
    }

    private void initDictionaryWithAscii() {
        for (int i = 0; i < INIT_DICTIONARY_SIZE; i++) {
            mDictionary.put(i, String.valueOf((char) i));
        }
    }


    private void readFile(String path) {
        Path pathToFile = Paths.get(path);
        try {
            mFile = Files.readAllBytes(pathToFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getCodesFromFile() {
        //Twelve bit fixed size: 00000100|1000 00011|1110100 => Three bytes get combined to two numbers
        byte temp1, temp2, temp3;
        int firstNumber, secondNumber;

        int limiter = mOddNumberOfCodes ? (mFile.length - 2) : (mFile.length);

        for (int i = 0; i < limiter; i = i + 3) {
            temp1 = mFile[i];
            temp2 = mFile[i + 1];
            temp3 = mFile[i + 2];

            firstNumber = (Byte.toUnsignedInt(temp1) << 4) + (Byte.toUnsignedInt(temp2) >> 4);
            secondNumber = ((Byte.toUnsignedInt(temp2) & 0b00001111) << 8) + (Byte.toUnsignedInt(temp3));

            mCodes.add(firstNumber);
            mCodes.add(secondNumber);
        }

        if (mOddNumberOfCodes) {
            temp1 = mFile[limiter];
            temp2 = mFile[limiter + 1];
            int number = (Byte.toUnsignedInt(temp1) << 8 + Byte.toUnsignedInt(temp2));
            mCodes.add(number);
        }
    }


    public void decodeFile() {
        //Used wikipedia and the explanation https://www.youtube.com/watch?v=mxqD315rYnA to teach the LZW algorithm myself
        getCodesFromFile();

        StringBuilder result = new StringBuilder();
        String lastOutput = "";
        String thisOutput = "";

        lastOutput = mDictionary.get(mCodes.get(0));
        result.append(lastOutput);

        for (int i = 1; i < mCodes.size(); i++) {
            int temp = mCodes.get(i);
            thisOutput = mDictionary.get(temp);
            if (thisOutput == null) {
                thisOutput = lastOutput + lastOutput.charAt(0);
            } else {
                thisOutput = mDictionary.get(temp);
            }

            pushToDictionary(lastOutput + thisOutput.charAt(0));

            result.append(mDictionary.get(temp));

            lastOutput = mDictionary.get(temp);
        }


        System.out.println(result.toString());
    }


    private void pushToDictionary(String val) {
        if (mDictionary.size() == Math.pow(2, 12)) {
            mDictionary = new HashMap<>();
            initDictionaryWithAscii();
        }
        mDictionary.put(mDictionary.size(), val);
    }


    //I also tried this, but this did not work out well and I chose a new approach, although the new one is not that
    //flexible when it comes to changing the word length (at least the simple one I implemented)
    //Failing is learning...

    /*private ArrayList<Integer> getIntegersFromByteArray(){
        ArrayList<Integer> list = new ArrayList<>();

        byte[] word = new byte[WORD_LENGTH_IN_BITS+1];

        int currentWordSize = 1;
        word[0]=0;

        while(true){
            if(currentWordSize == WORD_LENGTH_IN_BITS){
                list.add(ByteBuffer.wrap(word).getInt());
                System.out.println(ByteBuffer.wrap(word).getInt());
                word = new byte[WORD_LENGTH_IN_BITS+1];
                word[0]=0;
                currentWordSize = 1;
            } else {
                word[currentWordSize] = getBit();
                currentWordSize++;
                currentBitPosition++;
            }
        }


    }

    private byte getBit() {
        int posByte = currentBitPosition/8;
        int posBit = currentBitPosition%8;
        byte valByte = mFile[posByte];
        int valInt = valByte>>(8-(posBit+1)) & 0x0001;

        if(valInt == 1) return 1;
        else return 0;
    }*/


}
